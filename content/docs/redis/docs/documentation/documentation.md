<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Concepts](#concepts)
    - [Possible Redis deployments](#possible-redis-deployments)
        - [Redis Standalone](#redis-standalone)
        - [Redis Replicated](#redis-replicated)
        - [Redis Sentinel (the only one used for Gitlab.com)](#redis-sentinel-the-only-one-used-for-gitlabcom)
        - [Redis Cluster](#redis-cluster)
    - [Replication](#replication)
    - [Persistence](#persistence)
        - [RDB](#rdb)
        - [AOF](#aof)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


# Concepts #

## Possible Redis deployments ##

### Redis Standalone ###

### Redis Replicated ###

### Redis Sentinel (the only one used for Gitlab.com) ###

https://redis.io/topics/sentinel

### Redis Cluster ###

https://redis.io/topics/cluster-tutorial

## Replication ##

## Persistence ##

https://redis.io/topics/persistence

### RDB ###

### AOF ###
