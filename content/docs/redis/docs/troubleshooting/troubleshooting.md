# Troubleshooting #

## Is redis running? ##

### Grafana dashboards ###

- [redis](https://dashboards.gitlab.net/d/redis-main/redis-overview?orgId=1&from=now-6h&to=now)
- [redis-cache](https://dashboards.gitlab.net/d/redis-cache-main/redis-cache-overview?orgId=1&from=now-6h&to=now)
- [redis-sidekiq](https://dashboards.gitlab.net/d/redis-sidekiq-main/redis-sidekiq-overview?orgId=1&from=now-6h&to=now)


Explore the dashboards. For example, there is a Grafana chart showing number of slowlog events in redis-sidekiq (not linking it here because the panel ID changes when Grafana dashboards are deployed).

### Prometheus/Thanos metrics ###

- [redis_up](https://thanos-query.ops.gitlab.net/graph?g0.range_input=1w&g0.expr=redis_up%20%3C%201&g0.tab=0)

### Directly on a redis host ###

* is redis up?

```shell
$ gitlab-ctl status
```

* can we dial redis?
```shell
$ telnet localhost 6379
```
* can we talk to redis via `redis-cli`?
```shell
REDIS_MASTER_AUTH=$(sudo grep ^masterauth /var/opt/gitlab/redis/redis.conf|cut -d\" -f2) /opt/gitlab/embedded/bin/redis-cli -a $REDIS_MASTER_AUTH info
```

# Failover and Recovery procedures #

## Accessing the Redis console ##

{{< hint danger >}}
Be extremely careful with Redis! There are commands such as KEYS or MONITOR that can lock Redis entirely without any warning. The application relies heavily on cache so locking Redis will result in an immediate downtime.
{{< /hint >}}

Redis admin password is stored in the omnibus cookbook secrets in GKMS, and it's deployed to gitlab config file: `/etc/gitlab/gitlab.rb` (this file then gets translated into multiple other config files, including `redis.conf`)

Interactive console:
```shell
$ export REDIS_MASTER_AUTH=$(sudo grep ^masterauth /var/opt/gitlab/redis/redis.conf|cut -d\" -f2); /opt/gitlab/embedded/bin/redis-cli -a $REDIS_MASTER_AUTH
```

or oneliners:
```shell
$ export REDIS_MASTER_AUTH=$(sudo grep ^masterauth /var/opt/gitlab/redis/redis.conf|cut -d\" -f2); /opt/gitlab/embedded/bin/redis-cli -a $REDIS_MASTER_AUTH slowlog get 10
```

## Building a new Redis server and starting replication

{{< hint info >}}
These instructions are for setting up Redis *Sentinel*: https://redis.io/topics/sentinel . Not for setting up Redis *Cluster*: https://redis.io/topics/cluster-tutorial
{{< /hint >}}

From time to time you may have to build (or rebuild) a redis cluster. While the omnibus documentation (https://docs.gitlab.com/ee/administration/high_availability/redis.html) says everything should start replicating by magic, it doesn't in our builds because we touch `/etc/gitlab/skip-autoreconfigure` on redis nodes, so that restarts during upgrades can be done in a more controlled fashion across multiple nodes.

So, after building the nodes, there are some manual steps to take:

1. On all nodes
    * `sudo gitlab-ctl reconfigure`
    * This will reconfigure/start up redis, but not sentinel
1. On all nodes, `sudo gitlab-ctl start sentinel`
    * Not sure why, but it's minor
1. On the replicas, start replicating from the master:
    1. REDIS_MASTER_AUTH=$(sudo grep ^masterauth /var/opt/gitlab/redis/redis.conf|cut -d\\" -f2)
    1. /opt/gitlab/embedded/bin/redis-cli -a $REDIS_MASTER_AUTH
    1. 127.0.0.1:6379> slaveof MASTER_IP 6379
    1. 127.0.0.1:6379> info replication

You're now expecting the replica to report something like:
```
role:slave
master_host:MASTER_IP
master_port:6379
```

If you run `info replication` on the master, you expect to see `role:master` and `connected_slaves:2`

### Discussion
Sentinel is supposed to control the replication configuration in redis.conf (the 'slaveof' configuration line); therefore, when omnibus creates redis.conf it really shouldn't add that configuration line, otherwise it and sentinel would end up fighting.  So new redis nodes created with omnibus installed will all think they're master, until they're told otherwise.  We do this above, and at that point, sentinel (connected to the master) becomes aware of the replicas, and starts managing their replication status.

It's a little chicken-and-egg, and humans need to be involved.  It should, however, be one-off at cluster build time.


# References #

- https://blog.octo.com/en/what-redis-deployment-do-you-need/
- https://lzone.de/cheat-sheet/Redis
- https://tech.trivago.com/2017/01/25/learn-redis-the-hard-way-in-production/
- https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/80
- https://gitlab.com/gitlab-com/gl-infra/scalability/issues/49
- https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/7199
- https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9414
